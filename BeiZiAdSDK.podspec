
Pod::Spec.new do |spec|

  spec.name         = "BeiZiAdSDK"
  spec.version      = "4.91.5.1"
  spec.summary      = "BeiZiAdSDK 广告平台, 用于请求广告的iOS SDK."

  spec.description  = <<-DESC
  支持开屏, banner, 原生, 激励, 插屏, 原生插屏, 自定义类型广告请求。
                   DESC
                   
  spec.homepage     = "https://gitee.com/adscope/beiziadsdk.git"

  spec.license      = { :type => 'MIT', :file => 'LICENSE' }

  spec.author             = { "lvjunxue" => "lvjunxue@hubcloud.com.cn" }

  spec.source       = { :git => "https://gitee.com/adscope/beiziadsdk.git", :tag => "#{spec.version}" }
  #spec.source       = { :git => "https://github.com/CocoaPods/Specs.git", :tag => "#{spec.version}" }

  spec.ios.deployment_target = '10.0'

  # spec.source_files  = "Classes", "Classes/**/*.{h,m}"
  # spec.exclude_files = "Classes/Exclude"
  # spec.public_header_files = "Classes/**/*.h"
  
  spec.default_subspec = ['BeiZiAdSDK']
  
  spec.subspec 'BeiZiAdSDK' do |ss|
    ss.vendored_frameworks = ['BeiZiSDK.xcframework', 'BeiZiFoundation.xcframework']
  end
  

  spec.libraries  = 'z', 'c++', 'sqlite3', 'xml2', 'c++abi' , 'iconv', 'c'
  spec.frameworks = 'AdSupport', 'AVFoundation', 'CoreData', 'CoreLocation', 'CoreMotion', 'CoreTelephony', 'QuartzCore', 'SystemConfiguration', 'Security', 'StoreKit', 'WebKit'
  spec.weak_frameworks = 'AppTrackingTransparency'
  
  
  # spec.pod_target_xcconfig = { 'GCC_PREPROCESSOR_DEFINITIONS' => '$(inherited) GPB_USE_PROTOBUF_FRAMEWORK_IMPORTS=1' }
  
  #spec.requires_arc = true
  # spec.user_target_xcconfig =   {'OTHER_LDFLAGS' => ['-lObjC']}
  #spec.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
  #spec.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
  
  # spec.xcconfig = {"OTHER_LDFLAGS" => "-ObjC"}
    # spec.xcconfig = {"OTHER_LDFLAGS" => "-ObjC"}
      # spec.xcconfig = {"OTHER_LDFLAGS" => "-ObjC"}
        # spec.xcconfig = {"OTHER_LDFLAGS" => "-ObjC"}
  spec.xcconfig = {
    "OTHER_LDFLAGS" => "-ObjC"
  }

end
