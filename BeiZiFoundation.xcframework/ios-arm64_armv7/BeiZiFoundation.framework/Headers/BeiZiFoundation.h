//
//  BeiZiFoundation.h
//  BeiZiFoundation
//
//  Created by Cookie on 2022/10/8.
//

#import <Foundation/Foundation.h>

//! Project version number for BeiZiFoundation.
FOUNDATION_EXPORT double BeiZiFoundationVersionNumber;

//! Project version string for BeiZiFoundation.
FOUNDATION_EXPORT const unsigned char BeiZiFoundationVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <BeiZiFoundation/PublicHeader.h>

#import <BeiZiFoundation/BeiZiNetworking.h>

